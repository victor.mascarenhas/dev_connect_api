const MSGS = {    
    'FILE_INVALID_FORMAT': "Por favor envie um arquivo .jpeg ou .png",
    'FILE_NOT_SENT': "Por favor envie o arquivo",
    'FILE_UPLOADED': "Arquivo enviado com sucesso!",
    'GENERIC_ERROR' : "Server Error",    
    'USER_404' : 'Usuário não encontrado',
    'WITHOUT_TOKEN' : 'Token não enviado',
    'INVALID_TOKEN' : 'Token inválido',
    'VALID_EMAIL' : 'Por favor, insira um email válido',
    'REQUIRED_PASSWORD': 'Por favor, insira sua senha',
    'WRONG_PASSORD' : 'Senha incorreta',
    'PASSWORD_VALIDATED' : 'Por favor, insira uma senha com 6 ou mais caracteres'
}

module.exports = MSGS