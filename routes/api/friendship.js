const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const MSGS = require('../../messages')
const auth = require('../../middleware/auth')

router.get('/', auth, async (req, res, next) => {
    try {
        const id = req.user.id
        const user = await User.findById(id).populate('friendships')
        res.json(user.friendships)
    } catch (err) {
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})


router.post('/', auth, async (req, res, next) => {
    try {
        const id = req.user.id
        const user = await User.findById(id).populate('friendships')
        user.friendships.push(id)
        await user.save().then(t => t.populate({path: 'friendships', select: 'name picture username'}).execPopulate())
        if(user.id){
            res.json(user.friendships)
        }
        
    } catch (err) {
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.delete('/', auth, async (req, res, next) => {
    try {
        const id = req.user.id
        data = req.body.id
        const user = await User.findById(id).populate('friendships')
        user.friendships.pull(data)
        await user.save()
        if(user.id){
            res.json(user.friendships)
        }
        
    } catch (err) {
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

module.exports = router;