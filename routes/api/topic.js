const express = require('express')
const router = express.Router()
const Topic = require('../../models/topic')
const MSGS = require('../../messages')

router.get('/', async (req, res, next) => {
    try {
       const topics = await Topic.find({})
        res.json(topics)
    } catch (err) {
        res.status(500).send({ "error": MSGS.GENERIC_ERROR})
    }
})


router.post('/', async (req, res, next) => {
    try {
       let topics = new Topic(req.body)
       await topics.save()
       if(topics.id) {
           res.json(topics);
       }
    } catch (err) {
        res.status(500).send({ "error": MSGS.GENERIC_ERROR})
    }
})

module.exports = router;