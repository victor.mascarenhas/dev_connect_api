const express = require('express')
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs');
const router = express.Router()
const User = require('../../models/user')
const MSGS = require('../../messages')
const auth = require('../../middleware/auth')
const file = require('../../middleware/file')

router.get('/', auth, async (req, res, next) => {
    try {
        const user = await User.find({})
        res.json(user)
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR})
    }
})

router.get('/:id', auth, async (req, res, next) => {
  try {
      const id = req.params.id
      const user = await User.findById(id)
      res.json(user)
  } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR})
  }
})

router.post('/', file, [
    check('email', `${MSGS.VALID_EMAIL}`).isEmail(),
    check('name').not().isEmpty(),
    check('username').not().isEmpty(),
    check('password', `${MSGS.PASSWORD_VALIDATED}`).isLength({ min:6 })
], async (req, res, next) => {
    try {
        let password = req.body.password

        const errors = validationResult(req)
        if(!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        } else {
            let user = new User(req.body)
            if (req.body.picture_name){
                user.picture = `user/${req.body.picture_name}`
              }
            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, salt);
            await user.save()
                if (user.id) {
                    res.json(user)
                }            
        }
    } catch (err) {
        console.log(err.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.patch('/:userId', auth, file, async (req, res, next) => {
    try {
      const id = req.params.userId
      const salt = await bcrypt.genSalt(10)
      let data = req.body
  
      if (req.body.picture_name){
        data.picture = `user/${req.body.picture_name}`
      }
      if(data.password){
        data.password = await bcrypt.hash(data.password, salt)
      }
      const update = { $set: data }
      const user = await User.findByIdAndUpdate(id, update, { new: true })
      if (user) {
        res.send(user)
      } else {
        res.status(404).send({ error: MSGS.USER_404 })
      }
    } catch (err) {
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })

module.exports = router;