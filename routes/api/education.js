const express = require('express');
const User = require('../../models/user');
const router = express.Router();
const auth = require('../../middleware/auth')
const MSGS = require('../../messages')


router.post('/', auth, async (req, res, next) => {
  try {
    const id = req.user.id
    const education = await User.findByIdAndUpdate(id , { $push: { education: req.body } }, { new: true })
    if (education) {
      res.json(education)
    } else {
      res.status(404).send({ "error": MSGS.USER_404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


router.delete('/', auth, async (req, res, next) => {
  try {
    const id = req.user.id
    const education = await User.findByIdAndUpdate(id, { $pull: { education: req.body } }, { new: true })
    if (education) {
      res.json(education)
    } else {
      res.status(404).send({ "error": MSGS.USER_404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

module.exports = router;