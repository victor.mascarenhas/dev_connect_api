const { app, server } = require('../server')
const request = require('supertest')
const mongoose = require('mongoose');

let TOKEN = null
let res = null

beforeAll(async() => {
    res = request(app)
    await res.post('/auth')
    .send({
        email: "teste@email.com",
        password: "teste123"
      }).then(response=> {
          TOKEN = response.body.token
      })
})

const data = {
    "name": "Nome teste3",
    "username": "Nome de usuário teste3",
    "password": "teste1234",
    "email": "teste3@email.com"
}

describe('GET /user', function () {
    it('responds with json list', function (done) {
        res.get('/user')
            .set('Accept', 'application/json')
            .set('x-auth-token', TOKEN)
            .expect('Content-type', /json/)
            .expect(200, done);
    });

     it('should post a new user', function (done) {
        res.post('/user')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-type', /json/)
            .expect(200)
            .end(function (err, res) {
                if (err) return done(err);
                done();
            })
    }) 
})

afterAll(async () => {
    server.close()
    await mongoose.connection.close()
  })