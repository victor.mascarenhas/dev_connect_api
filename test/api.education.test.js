const { app, server} = require('../server')
const request = require('supertest');
const mongoose = require('mongoose');
let res = null
let TOKEN = ""


beforeAll(async () =>{
  res = request(app)
  await res.post('/auth/')
    .send({
      email: "teste2@email.com",
      password: "teste1234"
    }).then(response => {
      TOKEN = response.body.token
  })
})

const data = {
  "current": true,
  "school": "UFF",
  "degree": "MAST",
  "fieldofstudy": "I.T.",
  "description": "koe",
  "from": "2015-09-01T00:00:00.00Z",
  "to": "2018-03-01T00:00:00.00Z"
}

describe('POST /education', function() {
    it('should add education to logged user', function(done) {
      res.post('/education').send(data)
        .set('Accept', 'application/json')
        .set('x-auth-token', TOKEN)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it('should delete a education from logged user', function(done) {
        res.delete('/education')
        .send({current : true})
          .set('Accept', 'application/json')
          .set('x-auth-token', TOKEN)
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
  });

afterAll(async ()=>{
    server.close()
    await mongoose.connection.close()
})