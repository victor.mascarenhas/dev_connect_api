const express = require('express')
const app = express()
const config = require('config')
var cors = require('cors')
const connectDB = require('./config/db')
var bodyparser = require('body-parser')
const fileUpload = require('express-fileupload');
const PORT = process.env.PORT || 4646

//Init Middleware
app.use(cors())
app.use(express.json())
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
app.use('/uploads', express.static('uploads'))

app.use(fileUpload({
    createParentPath: true
}));

//Mongo Connect
//const db = config.get('mongoTestURI')
connectDB()//(db)

//Define Routes
app.get('/', (req, res) => res.send('Hello!'))
app.use('/topic', require('./routes/api/topic'))
app.use('/education', require('./routes/api/education'))
app.use('/friends', require('./routes/api/friendship'))
app.use('/auth', require('./routes/api/auth'))
app.use('/user', require('./routes/api/user'))


const server = app.listen(PORT, () => {console.log(`Listening on: ${PORT}`)})

module.exports = {app , server}